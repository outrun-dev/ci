package types

import (
	"errors"
	"fmt"
)

// Project is the top level data structure that encapsulates
// all other data in the CI system. Every other type must have
// a parent project at some point up its relationship tree.
type Project struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`

	User        User       `json:"user"`
	Group       Group      `json:"group"`
	Permissions Permission `json:"permissions"`

	GitRemotes []GitRemote `json:"git_remotes"`
}

// GitRemote is the remote location of a Git repository, specified
// by the URL and reference name.
type GitRemote struct {
	URL string `json:"url"`
	Ref string `json:"ref"`

	ProjectID string `json:"project_id,omitempty"`
}

// User is an entity that's authorized to interact with the CI system.
type User struct {
	Name     string `json:"name,omitempty"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`

	Groups []Group `json:"groups,omitempty"`
}

// Group is an aggregate of users to make things like assigning permissions
// to multiple users easer.
type Group struct {
	Name string `json:"name"`

	Users []User `json:"users,omitempty"`
}

// Permission is a byte encoding of the possible permissions that
// can be assigned to a project. It's in the following format:
//
// 0------7
// rwxrwx--
//
// Bits 0-2 apply to the user's group. Bits 3-5 apply to the public.
// The remaining two bits are for extensibility. "r" for "read", "w"
// for "write" and "x" for "execute".
type Permission byte

// UnmarshalJSON unmarshal's a JSON byte buffer into a Permission
// type. This is implemented on Permission to allow users of this
// type to specify permission bits as strings like "10110000".
func (p *Permission) UnmarshalJSON(buf []byte) error {
	// This handles zero values gracefully.
	if buf == nil || (buf[0] == '0' && len(buf) == 1) {
		perm := Permission(0x0)
		*p = perm

		return nil
	}

	if buf[0] != '"' || buf[len(buf)-1] != '"' {
		return errors.New("input to UnmarshalJSON not a valid JSON string")
	}

	var b byte
	_, err := fmt.Sscanf(string(buf[1:len(buf)-1]), "%b", &b)
	if err != nil {
		return err
	}

	perm := Permission(b)
	*p = perm

	return nil
}

// MarshalJSON marshals a Permission type into the appropriate JSON
// value as a byte buffer.
func (p *Permission) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%08b"`, *p)), nil
}
