package types

import "errors"

var (
	// ErrProjectNotFound is returned by anything that can return
	// a project, but no project was found that met the given
	// criteria.
	ErrProjectNotFound = errors.New("project not found")

	// ErrGitRemoteExists is returned by anything that can put a git remote
	// on a project, but the remote is already configured for that project.
	ErrGitRemoteExists = errors.New("git remote already exists for project")
)
