module gitlab.com/outrun-dev/ci

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.0.0
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/crypto v0.0.0-20190404164418-38d8ce5564a5
)
