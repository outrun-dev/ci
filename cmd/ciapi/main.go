package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/outrun-dev/ci/db"
	"gitlab.com/outrun-dev/ci/http"
)

var logger *logrus.Entry

func init() {
	logger = getLogger()
}

func main() {
	logger.Info("booting ci server")

	connstr, err := getDBConnStr()
	if err != nil {
		logger.WithError(err).
			Fatal("unable to get database connection string")
	}

	pg, err := db.NewPostgres(connstr)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to database")
	}

	jwtsecret, err := getJWTSecret()
	if err != nil {
		logger.WithError(err).Fatal("unable to get jwt secret")
	}

	logger.WithError(http.NewServer(":9001", pg, jwtsecret).ListenAndServe()).
		Fatal("got error serving HTTP")
}

func getLogger() *logrus.Entry {
	rawLogLevel := os.Getenv("CIAPI_LOG_LEVEL")
	if rawLogLevel == "" {
		rawLogLevel = "info"
	}

	logLevel, err := logrus.ParseLevel(rawLogLevel)
	if err != nil {
		logrus.Warn("unable to parse log level, defaulting to INFO")
		logLevel = logrus.InfoLevel
	}
	logrus.SetLevel(logLevel)

	logger := logrus.WithFields(logrus.Fields{})
	logger.Debugf("setting level to %v", logLevel)

	return logger
}

func getDBConnStr() (string, error) {
	pguser := os.Getenv("CIAPI_POSTGRES_USER")
	if pguser == "" {
		return "", errors.New("need CIAPI_POSTGRES_USER")
	}

	pgpass := os.Getenv("CIAPI_POSTGRES_PASS")
	if pgpass == "" {
		return "", errors.New("need CIAPI_POSTGRES_PASS")
	}

	pghref := os.Getenv("CIAPI_POSTGRES_HREF")
	if pghref == "" {
		return "", errors.New("need CIAPI_POSTGRES_HREF")
	}

	pgdb := os.Getenv("CIAPI_POSTGRES_DB")
	if pgdb == "" {
		return "", errors.New("need CIAPI_POSTGRES_DB")
	}

	pgssl := os.Getenv("CIAPI_POSTGRES_SSL")
	if pgssl == "" {
		// Secure by default
		pgssl = "verify-full"
	}

	return fmt.Sprintf("postgres://%v:%v@%v/%v?sslmode=%v",
		pguser, pgpass, pghref, pgdb, pgssl), nil
}

func getJWTSecret() (string, error) {
	secret := os.Getenv("CIAPI_JWT_SECRET")
	if secret == "" {
		return "", errors.New("need CIAPI_JWT_SECRET")
	}

	return secret, nil
}
