package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"

	"gitlab.com/outrun-dev/ci/types"
)

func TestCreateProject(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to save the project.
		createProject: func(spec types.Project) (types.Project, error) {
			spec.ID = "a"

			return spec, nil
		},
	}, "")

	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	type result struct {
		status int
		data   types.Project
	}

	tests := []struct {
		input    types.Project
		expected result
	}{
		{
			types.Project{
				Name: "test",
			},
			result{
				status: http.StatusOK,
				data: types.Project{
					Name: "test",
					ID:   "a",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.input.Name, func(t *testing.T) {
			reqbody, err := json.Marshal(test.input)

			url := fmt.Sprintf("%v/projects", testsrv.URL)
			req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqbody))
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			err = setReqAuth(req, srv.jwtsecret)
			if err != nil {
				t.Fatalf("got error setting request bearer token: %v", err)
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}
			defer resp.Body.Close()

			respbody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("expected no error reading response body, got %v", err)
			}

			actual := result{
				status: resp.StatusCode,
			}
			err = json.Unmarshal(respbody, &actual.data)
			if err != nil {
				t.Fatalf("expected no error unmarshaling response body, got %v", err)
			}

			if test.expected.status != actual.status {
				t.Fatalf("expected status %v, got %v", test.expected.status, actual.status)
			}

			if test.expected.data.ID != actual.data.ID {
				t.Fatalf("expected project id to be %v, got %v", test.expected.data.ID, actual.data.ID)
			}

			if test.expected.data.Name != actual.data.Name {
				t.Fatalf("expected project name to be %v, got %v", test.expected.data.Name, actual.data.Name)
			}
		})
	}
}

func TestListProjects(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	projects := map[string]types.Project{
		"a": types.Project{
			Name:        "test-1",
			ID:          "a",
			Permissions: types.Permission(0xA0),
			Group: types.Group{
				Name: "foo",
			},
			User: types.User{
				Email: "test@local",
			},
		},

		"b": types.Project{
			Name:        "test-2",
			ID:          "b",
			Permissions: types.Permission(0xA0),
			Group: types.Group{
				Name: "bar",
			},
			User: types.User{
				Email: "test@local",
			},
		},
	}

	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to list the projects.
		listProjects: func(user string) ([]types.Project, error) {
			projs := []types.Project{}

			for _, p := range projects {
				if p.User.Email == user {
					projs = append(projs, p)
				}
			}

			return projs, nil
		},
	}, "")

	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	url := fmt.Sprintf("%v/projects", testsrv.URL)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fatalf("expected no error creating test request, got %v", err)
	}
	err = setReqAuth(req, srv.jwtsecret)
	if err != nil {
		t.Fatalf("got error setting request bearer token: %v", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("expected no error making request to test server, got %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Fatalf("expected status %v, got %v", http.StatusOK, resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("expected no error reading response body, got %v", err)
	}

	var actual []types.Project
	err = json.Unmarshal(body, &actual)
	if err != nil {
		t.Fatalf("expected no error unmarshaling json, got %v", err)
	}

	if len(actual) != len(projects) {
		t.Fatalf("expected to get projects %+v\n\ngot %+v\n\n", projects, actual)
	}

	for _, p := range actual {
		if proj, ok := projects[p.ID]; !ok {
			t.Fatalf("got unexpected project with id %v: %+v", p.ID, proj)
		} else if proj.Description != p.Description ||
			proj.Name != p.Name ||
			proj.Permissions != p.Permissions ||
			proj.Group.Name != p.Group.Name ||
			proj.User.Email != p.User.Email {
			t.Fatalf("expected project %+v\n\ngot %+v", proj, p)
		}
	}
}
