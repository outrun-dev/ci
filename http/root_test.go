package http

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoot(t *testing.T) {
	srv := NewServer("", &testStore{}, "")
	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	// Testing all the HTTP methods here tests the allowMethods middleware.
	tests := []struct {
		method string
		status int
	}{
		{
			http.MethodGet,
			http.StatusNoContent,
		},
		{
			http.MethodHead,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodDelete,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodOptions,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodPatch,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodPost,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodPut,
			http.StatusMethodNotAllowed,
		},
		{
			http.MethodTrace,
			http.StatusMethodNotAllowed,
		},
	}

	for _, test := range tests {
		t.Run(test.method, func(t *testing.T) {
			req, err := http.NewRequest(test.method, testsrv.URL, nil)
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}

			if resp.StatusCode != test.status {
				t.Fatalf("expected status %v, got %v", test.status, resp.StatusCode)
			}
		})
	}
}
