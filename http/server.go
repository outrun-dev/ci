package http

import (
	"net/http"

	"gitlab.com/outrun-dev/ci/types"
)

type store interface {
	CreateProject(spec types.Project) (types.Project, error)
	ListProjects(user string) ([]types.Project, error)
	GetProject(user, id string) (types.Project, error)
	DeleteProject(id string) error

	CreateGitRemote(user string, remote types.GitRemote) error

	Authenticate(user, pass string) error
}

// Server is an HTTP server tailored for serving ci requests.
type Server struct {
	*http.Server

	db store

	jwtsecret []byte
}

// NewServer instantiates a ci server configured to listen at
// the given address.
func NewServer(addr string, db store, jwtsecret string) *Server {
	srv := &Server{
		Server: &http.Server{
			Addr: addr,
		},

		db: db,

		jwtsecret: []byte(jwtsecret),
	}

	srv.Handler = srv.getMux()

	return srv
}

// This function exists to make testing easier. This way the mux
// can easily be plugged into the test server.
func (srv Server) getMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.Handle("/", Chain(
		handleRoot,
		setRequestID,
		logRequest,
		allowMethods(http.MethodGet)))

	mux.Handle("/projects", Chain(
		srv.handleProjects,
		setRequestID,
		logRequest,
		srv.checkAuth,
		allowMethods(http.MethodPost, http.MethodGet)))

	mux.Handle("/projects/", Chain(
		srv.handleProject,
		setRequestID,
		logRequest,
		srv.checkAuth,
		allowMethods(http.MethodGet, http.MethodDelete)))

	mux.Handle("/git_remotes", Chain(
		srv.handleGitRemotes,
		setRequestID,
		logRequest,
		srv.checkAuth,
		allowMethods(http.MethodPost)))

	mux.Handle("/auth", Chain(
		srv.handleAuth,
		setRequestID,
		logRequest,
		allowMethods(http.MethodPost),
	))

	return mux
}
