package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"gitlab.com/outrun-dev/ci/types"
	"github.com/sirupsen/logrus"
)

func (srv *Server) handleProject(rw http.ResponseWriter, req *http.Request) {
	segs := strings.Split(req.URL.EscapedPath(), "/")

	// This tests if the ID got set to nothing, including whitespace, for
	// whatever reason. AFAIK the second condition technically is impossible.
	if len(segs) < 3 || strings.TrimSpace(segs[2]) == "" {
		err := errors.New("must specify project ID in request path")
		logger.WithError(err).Error("unable to projects request for project")
		writeErrResp(rw, err, http.StatusBadRequest)
		return
	}

	switch req.Method {
	case http.MethodGet:
		handleGetProject(rw, req, srv.db, segs[2])
	case http.MethodDelete:
		handleDeleteProject(rw, req, srv.db, segs[2])
	}
}

func handleGetProject(rw http.ResponseWriter, req *http.Request, db store, id string) {
	reqID := req.Context().Value(keyReqID).(string)
	reqSub := req.Context().Value(keyReqSub).(string)
	logger := logger.WithFields(logrus.Fields{
		"request_id":      reqID,
		"request_subject": reqSub,
	})

	logger.Debug("handling get project")

	proj, err := db.GetProject(reqSub, id)
	if err == types.ErrProjectNotFound {
		logger.WithError(err).Error("unable to get project from database")
		writeErrResp(rw, err, http.StatusNotFound)
		return
	}
	if err != nil {
		logger.WithError(err).Error("unable to get project from database")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	buf, err := json.Marshal(proj)
	if err != nil {
		logger.WithError(err).Error("unable to marshal response json")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write(buf)
	return
}

func handleDeleteProject(rw http.ResponseWriter, req *http.Request, db store, id string) {
	reqID := req.Context().Value(keyReqID).(string)
	logger := logger.WithFields(logrus.Fields{
		"request_id": reqID,
		"project_id": id,
	})

	logger.Debug("handling delete project")

	err := db.DeleteProject(id)
	if err == types.ErrProjectNotFound {
		logger.WithError(err).Error("unable to get project from database")
		writeErrResp(rw, err, http.StatusNotFound)
		return
	}
	if err != nil {
		logger.WithError(err).Error("unable to get project from database")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusNoContent)
	return
}
