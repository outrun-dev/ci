package http

import (
	"context"
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type ctxkey int

const (
	keyReqID ctxkey = iota
	keyReqSub
)

// Middleware is a function that can intercept the handling of an HTTP request
// to do something useful.
type Middleware func(http.HandlerFunc) http.HandlerFunc

// Chain builds the final http.Handler from all the middlewares passed to it.
func Chain(f http.HandlerFunc, mw ...Middleware) http.Handler {
	// Because function calls are placed on a stack, they need to
	// be applied in reverse order from what they are passed in,
	// in order for calls to Chain() to be intuitive.
	for i := len(mw) - 1; i >= 0; i-- {
		f = mw[i](f)
	}

	return f
}

func setRequestID(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		id := uuid.New().String()

		ctx := context.WithValue(req.Context(), keyReqID, id)
		logger.WithField("request_id", id).
			Debug("setting request ID")

		f(rw, req.WithContext(ctx))
	}
}

func logRequest(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		reqid := req.Context().Value(keyReqID).(string)

		logger := logger.WithField("request_id", reqid)

		logger.Infof("%v %v", req.Method, req.URL)

		f(rw, req)
	}
}

func allowMethods(methods ...string) Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(rw http.ResponseWriter, req *http.Request) {
			reqID := req.Context().Value(keyReqID).(string)
			logger := logger.WithFields(logrus.Fields{
				"request_id": reqID,
			})

			for _, method := range methods {
				if strings.ToLower(method) == strings.ToLower(req.Method) {
					logger.Debugf("matched method %v", method)
					f(rw, req)
					return
				}
			}

			logger.Debug("failed to match any methods for request")
			rw.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
	}
}

func (srv *Server) checkAuth(f http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		hdrline, ok := req.Header["Authorization"]
		if !ok {
			err := errors.New("missing bearer token")

			logger.WithError(err).Error("unable to authorize request")
			rw.WriteHeader(http.StatusUnauthorized)
			return
		}

		hdr := strings.Split(hdrline[0], " ")

		if len(hdr) < 2 {
			err := errors.New("missing bearer token")

			logger.WithError(err).Error("unable to authorize request")
			rw.WriteHeader(http.StatusUnauthorized)
			return
		}

		// Tokens come in the form of "Bearer $TOKEN"
		bearer := hdr[1]

		keyfn := func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				err := errors.New("invalid signing method for bearer token")

				return nil, err
			}

			return srv.jwtsecret, nil
		}

		token, err := jwt.ParseWithClaims(bearer, &jwt.StandardClaims{}, keyfn)
		if err != nil {
			logger.WithError(err).Error("unable to authorize request")
			rw.WriteHeader(http.StatusUnauthorized)
			return
		}

		if claims, ok := token.Claims.(*jwt.StandardClaims); ok && token.Valid {
			if time.Now().Unix() > claims.ExpiresAt {
				err := errors.New("token expired")
				logger.WithError(err).Error("unable to authorize request")
				rw.WriteHeader(http.StatusUnauthorized)
				return
			}

			ctx := context.WithValue(req.Context(), keyReqSub, claims.Subject)
			logger.WithField("sub", claims.Subject).
				Debug("setting auth subject")

			f(rw, req.WithContext(ctx))
			return
		}

		err = errors.New("invalid bearer token")
		logger.WithError(err).Error("unable to authorize request")
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}
}
