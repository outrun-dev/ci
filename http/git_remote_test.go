package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/outrun-dev/ci/types"
)

func TestCreateGitRemote(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	projects := map[string]types.Project{
		"a": types.Project{
			Name: "test-1",
			ID:   "a",
			User: types.User{
				Email: "test@local",
			},
		},
		"b": types.Project{
			Name: "test-2",
			ID:   "b",
			User: types.User{
				Email: "test@local",
			},

			GitRemotes: []types.GitRemote{
				types.GitRemote{
					URL: "git.local/bar.git",
					Ref: "master",

					ProjectID: "b",
				},
			},
		},
	}

	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to save the git remote.
		createGitRemote: func(user string, remote types.GitRemote) error {
			proj, ok := projects[remote.ProjectID]
			if !ok {
				return types.ErrProjectNotFound
			}

			for _, r := range proj.GitRemotes {
				if r.URL == remote.URL && r.Ref == remote.Ref {
					return types.ErrGitRemoteExists
				}
			}

			proj.GitRemotes = append(proj.GitRemotes)
			return nil
		},
	}, "")

	var testsrv *httptest.Server
	testsrv = httptest.NewServer(srv.getMux())
	defer testsrv.Close()

	type result struct {
		status int
		data   interface{}
	}

	tests := []struct {
		input    types.GitRemote
		expected result
	}{
		{
			types.GitRemote{
				ProjectID: "a",
				URL:       "git.local/foo.git",
				Ref:       "master",
			},
			result{
				http.StatusNoContent,
				nil,
			},
		},
		// A duplicate should fail for the same project
		{
			projects["b"].GitRemotes[0],
			result{
				http.StatusConflict,
				map[string]string{
					"error": "git remote already exists for project",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v#%v", test.input.URL, test.input.Ref), func(t *testing.T) {
			reqbody, err := json.Marshal(test.input)

			url := fmt.Sprintf("%v/git_remotes", testsrv.URL)
			req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqbody))
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			err = setReqAuth(req, srv.jwtsecret)
			if err != nil {
				t.Fatalf("got error setting request bearer token: %v", err)
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}
			defer resp.Body.Close()

			respbody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("expected no error reading response body, got %v", err)
			}

			actual := result{
				status: resp.StatusCode,
			}
			err = json.Unmarshal(respbody, &actual.data)
			if err != nil && test.expected.data != nil {
				t.Fatalf("expected no error unmarshaling response body, got %v", err)
			}

			if test.expected.status != actual.status {
				t.Fatalf("expected status %v, got %v", test.expected.status, actual.status)
			}

			if test.expected.data != nil {
				switch val := actual.data.(type) {
				case map[string]interface{}:
					expected := test.expected.data.(map[string]string)
					if len(val) != len(expected) {
						t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
					}

					for k, v := range val {
						exp, ok := expected[k]
						if !ok {
							t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
						}

						msg, ok := v.(string)
						if !ok {
							t.Fatalf("got unknown value type for key %v: %v (%T)", k, v, v)
						}

						if msg != exp {
							t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
						}
					}
				default:
					t.Fatalf("unexpected type returned for %v: %T", val, val)
				}
			}
		})
	}
}
