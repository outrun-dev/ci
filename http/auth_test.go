package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/outrun-dev/ci/types"
)

func TestAuth(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	u := types.User{
		Email:    "test@example.com",
		Password: "foobar",
	}

	jwtsecret := []byte("valid")
	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to authenticate.
		authenticate: func(user, pass string) error {
			if user == u.Email && pass == u.Password {
				return nil
			}

			return errNotAuthenticated
		},
	}, string(jwtsecret))

	type result struct {
		status    int
		emptyBody bool
	}
	tests := []struct {
		user     string
		pass     string
		expected result
	}{
		{
			u.Email,
			u.Password,
			result{
				http.StatusOK,
				false,
			},
		},
		{
			"foo",
			"bar",
			result{
				http.StatusUnauthorized,
				true,
			},
		},
		{
			"",
			"",
			result{
				http.StatusUnauthorized,
				true,
			},
		},
	}

	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v:%v", test.user, test.pass), func(t *testing.T) {
			reqbody, err := json.Marshal(map[string]string{
				"email":    test.user,
				"password": test.pass,
			})
			if err != nil {
				t.Fatalf("error marshaling request body: %v", err)
			}

			url := fmt.Sprintf("%v/auth", testsrv.URL)
			req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(reqbody))
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}
			defer resp.Body.Close()

			if test.expected.status != resp.StatusCode {
				t.Fatalf("expected status %v, got %v", test.expected.status, resp.StatusCode)
			}

			respbody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("expected no error reading response body, got %v", err)
			}

			if (len(respbody) == 0) != test.expected.emptyBody {
				t.Fatalf("expected emtpy body %v, got %s", test.expected.emptyBody, respbody)
			}

			// If the above test passed and we have no body, there's nothing left to see here.
			if len(respbody) == 0 {
				return
			}

			data := map[string]string{}
			err = json.Unmarshal(respbody, &data)
			if err != nil {
				t.Fatalf("error unmarshaling response body: %v", err)
			}

			if tokenString, ok := data["token"]; !ok {
				t.Fatalf("expected response body to have a 'token'\n\ngot: %+v\n\n", data)
			} else if token, err := jwt.Parse(tokenString, getTokenFunc(jwtsecret)); err != nil {
				t.Fatalf("expected response body to have a 'token'\n\ngot: %+v\n\n", data)
			} else if claims, ok := token.Claims.(jwt.MapClaims); !ok || !token.Valid {
				t.Fatalf("expected token claims to be ok, got %v %v", ok, token.Valid)
			} else if claims["exp"] == int64(0) {
				t.Fatal("expected token expiration to be set")
			} else if claims["iss"] != tokenIssuer {
				t.Fatalf("expected token issuer %v, got %v", tokenIssuer, claims["iss"])
			} else if claims["sub"] != test.user {
				t.Fatalf("expected token subject %v, got %v", u.Email, claims["sub"])
			}
		})
	}
}

func getTokenFunc(jwtsecret []byte) func(token *jwt.Token) (interface{}, error) {
	return func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return jwtsecret, nil
	}
}
