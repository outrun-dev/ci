package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/outrun-dev/ci/types"
	"github.com/sirupsen/logrus"
)

type createProjectRequest struct {
	types.Project

	GroupName string `json:"group_name"`
}

func (srv *Server) handleProjects(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		handleCreateProjects(rw, req, srv.db)
	case http.MethodGet:
		handleListProjects(rw, req, srv.db)
	}
}

func handleListProjects(rw http.ResponseWriter, req *http.Request, db store) {
	reqID := req.Context().Value(keyReqID).(string)
	reqSub := req.Context().Value(keyReqSub).(string)
	logger := logger.WithFields(logrus.Fields{
		"request_id":      reqID,
		"request_subject": reqSub,
	})

	logger.Debug("handling list projects")

	projects, err := db.ListProjects(reqSub)
	if err != nil {
		logger.WithError(err).Error("unable to list projects from database")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	buf, err := json.Marshal(projects)
	if err != nil {
		logger.WithError(err).Error("unable to marshal projects")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write(buf)
	return
}

func handleCreateProjects(rw http.ResponseWriter, req *http.Request, db store) {
	reqID := req.Context().Value(keyReqID).(string)
	reqSub := req.Context().Value(keyReqSub).(string)
	logger := logger.WithFields(logrus.Fields{
		"request_id":      reqID,
		"request_subject": reqSub,
	})

	logger.Debug("handling create projects")

	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	var data createProjectRequest
	err = json.Unmarshal(buf, &data)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal request body")
		writeErrResp(rw, err, http.StatusBadRequest)
		return
	}

	logger.Debugf("got data project permissions %v", data.Permissions)

	spec := types.Project{
		Name:        data.Name,
		Description: data.Description,

		Group: types.Group{
			Name: data.GroupName,
		},

		User: types.User{
			Email: reqSub,
		},

		Permissions: data.Permissions,
	}

	logger.Debugf("got project permissions %v", spec.Permissions)

	proj, err := db.CreateProject(spec)
	if err != nil {
		logger.WithError(err).Error("unable to save project in database")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	buf, err = json.Marshal(proj)
	if err != nil {
		logger.WithError(err).Error("unable to marshal saved project")
		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write(buf)
	return
}
