package http

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/outrun-dev/ci/types"
)

type testStore struct {
	createProject func(types.Project) (types.Project, error)
	listProjects  func(string) ([]types.Project, error)
	getProject    func(string, string) (types.Project, error)
	deleteProject func(string) error

	createGitRemote func(string, types.GitRemote) error

	authenticate func(string, string) error
}

func (db *testStore) CreateProject(spec types.Project) (types.Project, error) {
	if db.createProject != nil {
		return db.createProject(spec)
	}

	return spec, errors.New("test method not implemented")
}

func (db *testStore) ListProjects(user string) ([]types.Project, error) {
	if db.listProjects != nil {
		return db.listProjects(user)
	}

	return nil, errors.New("test method not implemented")
}

func (db *testStore) GetProject(user, id string) (types.Project, error) {
	if db.getProject != nil {
		return db.getProject(user, id)
	}

	return types.Project{}, errors.New("test method not implemented")
}

func (db *testStore) DeleteProject(id string) error {
	if db.deleteProject != nil {
		return db.deleteProject(id)
	}

	return errors.New("test method not implemented")
}

func (db *testStore) CreateGitRemote(user string, remote types.GitRemote) error {
	if db.createGitRemote != nil {
		return db.createGitRemote(user, remote)
	}

	return errors.New("test method not implemented")
}

func (db *testStore) Authenticate(user, pass string) error {
	if db.authenticate != nil {
		return db.authenticate(user, pass)
	}

	// Returning nil here so that auth can just continue as normal.
	return nil
}

func setReqAuth(req *http.Request, secret []byte) error {
	ss, err := getSignedToken("test@local", secret)
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", ss))
	return nil
}
