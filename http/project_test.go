package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/outrun-dev/ci/types"
)

func TestGetProject(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	projects := map[string]types.Project{
		"a": types.Project{
			Name: "test-1",
			ID:   "a",
			User: types.User{
				Email: "test@local",
			},
		},
		"b": types.Project{
			Name: "test-2",
			ID:   "b",
			User: types.User{
				Email: "test@local",
			},
		},
	}

	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to read the project.
		getProject: func(user, id string) (types.Project, error) {
			proj, ok := projects[id]
			if !ok || proj.User.Email != user {
				return types.Project{}, types.ErrProjectNotFound
			}

			return proj, nil
		},
	}, "")

	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	type result struct {
		status int
		data   interface{}
	}
	tests := []struct {
		input    string
		expected result
	}{
		{
			"a",
			result{
				http.StatusOK,
				projects["a"],
			},
		},
		{
			"b",
			result{
				http.StatusOK,
				projects["b"],
			},
		},
		{
			"c",
			result{
				http.StatusNotFound,
				map[string]string{
					"error": "project not found",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			url := fmt.Sprintf("%v/projects/%v", testsrv.URL, test.input)
			req, err := http.NewRequest(http.MethodGet, url, nil)
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			err = setReqAuth(req, srv.jwtsecret)
			if err != nil {
				t.Fatalf("got error setting request bearer token: %v", err)
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}
			defer resp.Body.Close()

			if resp.StatusCode != test.expected.status {
				t.Fatalf("expected status %v, got %v", test.expected.status, resp.StatusCode)
			}

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("expected no error reading response body, got %v", err)
			}

			var data interface{}
			err = json.Unmarshal(body, &data)
			if err != nil {
				t.Fatalf("expected no error unmarshaling json, got %v", err)
			}

			switch val := data.(type) {
			case types.Project:
				expected := test.expected.data.(types.Project)
				if val.ID != expected.ID {
					t.Fatalf("expected project with id %v, got %v", expected.ID, val.ID)
				}

				if val.Name != expected.Name {
					t.Fatalf("expected project with name %v, got %v", expected.Name, val.Name)
				}

			// TODO: fix this, the type of this is map[string]interface{} (see the delete test)
			case map[string]string:
				expected := test.expected.data.(map[string]string)
				if len(val) != len(expected) {
					t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
				}

				for k, v := range val {
					if exp, ok := expected[k]; !ok {
						t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
					} else if v != exp {
						t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
					}
				}
			}
		})
	}
}

func TestDeleteProject(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	projects := map[string]types.Project{
		"a": types.Project{
			Name: "test-1",
			ID:   "a",
		},
		"b": types.Project{
			Name: "test-2",
			ID:   "b",
		},
	}

	srv := NewServer("", &testStore{
		// Setting this just tests that the server's database is called
		// to delete the project.
		deleteProject: func(id string) error {
			if _, ok := projects[id]; !ok {
				return types.ErrProjectNotFound
			}

			delete(projects, id)
			return nil
		},
	}, "")

	testsrv := httptest.NewServer(srv.getMux())
	defer srv.Close()

	type result struct {
		status int
		data   interface{}
	}
	tests := []struct {
		input    string
		expected result
	}{
		{
			"a",
			result{
				http.StatusNoContent,
				nil,
			},
		},
		{
			"b",
			result{
				http.StatusNoContent,
				nil,
			},
		},
		{
			// Deleting this key that has already been deleted should
			// return an error saying the project wasn't found.
			"b",
			result{
				http.StatusNotFound,
				map[string]string{
					"error": "project not found",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			url := fmt.Sprintf("%v/projects/%v", testsrv.URL, test.input)
			req, err := http.NewRequest(http.MethodDelete, url, nil)
			if err != nil {
				t.Fatalf("expected no error creating test request, got %v", err)
			}
			err = setReqAuth(req, srv.jwtsecret)
			if err != nil {
				t.Fatalf("got error setting request bearer token: %v", err)
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("expected no error making request to test server, got %v", err)
			}
			defer resp.Body.Close()

			if resp.StatusCode != test.expected.status {
				t.Fatalf("expected status %v, got %v", test.expected.status, resp.StatusCode)
			}

			if test.expected.data != nil {
				body, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					t.Fatalf("expected no error reading response body, got %v", err)
				}

				var data interface{}
				err = json.Unmarshal(body, &data)
				if err != nil {
					t.Fatalf("expected no error unmarshaling json, got %v", err)
				}

				switch val := data.(type) {
				case map[string]interface{}:
					expected := test.expected.data.(map[string]string)
					if len(val) != len(expected) {
						t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
					}

					for k, v := range val {
						exp, ok := expected[k]
						if !ok {
							t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
						}

						msg, ok := v.(string)
						if !ok {
							t.Fatalf("got unknown value type for key %v: %v (%T)", k, v, v)
						}

						if msg != exp {
							t.Fatalf("expected response %+v\n\ngot %+v\n\n", expected, val)
						}
					}
				default:
					t.Fatalf("unexpected type returned for %v: %T", val, val)
				}
			}
		})
	}
}
