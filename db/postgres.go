package db

import (
	"database/sql"
	"errors"

	"gitlab.com/outrun-dev/ci/types"
	"golang.org/x/crypto/bcrypt"

	_ "github.com/lib/pq" // Import the PostgreSQL driver.
)

// Postgres is a PostgreSQL database that's also a PipelineStore.
type Postgres struct {
	db *sql.DB
}

// NewPostgres returns a Postgres database handle.
func NewPostgres(connstr string) (*Postgres, error) {
	logger = logger.WithField("db", "postgres")

	logger.Debug("connecting to database")

	db, err := sql.Open("postgres", connstr)
	if err != nil {
		logger.WithError(err).Debug("unable to connect to database")
		return nil, err
	}

	return &Postgres{
		db: db,
	}, nil
}

// CreateProject saves a Project with the given spec in the database. It returns
// the saved Project.
func (pg *Postgres) CreateProject(spec types.Project) (types.Project, error) {
	logger := logger.WithField("project", spec.Name)
	logger.Debug("saving project to postgres")

	logger.Debugf("project perms %v", spec.Permissions)

	sqlinsert := `
	INSERT INTO projects (name, description, user_email, group_name, permissions)
	VALUES
		($1, $2, $3, $4, $5)
	RETURNING id;
	`

	// Using QueryRow because the insert is returning the ID.
	err := pg.db.QueryRow(sqlinsert,
		spec.Name,
		spec.Description,
		spec.User.Email,
		spec.Group.Name,
		spec.Permissions,
	).Scan(&spec.ID)

	if err != nil {
		logger.WithError(err).
			Debug("unable to create project")
	}
	return spec, err
}

// ListProjects lists all the projects in the database.
func (pg *Postgres) ListProjects(user string) ([]types.Project, error) {
	logger.Debug("listing projects in postgres")

	sqlq := `
	SELECT proj.id, proj.name, proj.description,
		proj.permissions, proj.user_email, proj.group_name
	FROM projects AS proj
	WHERE proj.user_email = $1
	OR proj.group_name IN (
		SELECT mapping.group_name
		FROM user_groups AS mapping
		WHERE mapping.user_email = $2
	);
	`

	rows, err := pg.db.Query(sqlq, user, user)
	if err != nil {
		logger.WithError(err).Debug("unable to query database")
		return nil, err
	}

	projects := []types.Project{}
	for rows.Next() {
		p := types.Project{}

		var desc sql.NullString
		err := rows.Scan(
			&p.ID,
			&p.Name,
			&desc,
			&p.Permissions,
			&p.User.Email,
			&p.Group.Name,
		)
		if err != nil {
			logger.WithError(err).Debug("unable to scan row")
			return projects, err
		}

		if desc.Valid {
			p.Description = desc.String
		}

		projects = append(projects, p)
	}

	return projects, nil
}

// GetProject returns a project with the given id in the database.
// If no project is found it returns `types.ErrProjectNotFound`.
func (pg *Postgres) GetProject(user, id string) (types.Project, error) {
	logger := logger.WithField("project_id", id)
	logger.Debug("retrieving project from postgres")

	tx, err := pg.db.Begin()
	if err != nil {
		logger.WithError(err).Debug("unable to begin transaction")
		return types.Project{}, err
	}

	sqlq := `
	SELECT proj.id, proj.name, proj.description,
		proj.permissions, proj.user_email, proj.group_name
	FROM projects AS proj
	WHERE proj.id = $1
	AND (
		proj.user_email = $2
		OR proj.group_name IN (
			SELECT mapping.group_name
			FROM user_groups AS mapping
			WHERE mapping.user_email = $3
		)
	);
	`
	var p types.Project
	var desc sql.NullString
	err = tx.QueryRow(sqlq, id, user, user).Scan(
		&p.ID,
		&p.Name,
		&desc,
		&p.Permissions,
		&p.User.Email,
		&p.Group.Name,
	)
	if err == sql.ErrNoRows {
		logger.WithError(err).Debug("unable to find project in database")
		return p, types.ErrProjectNotFound
	}
	if err != nil {
		logger.WithError(err).Debug("unable to query database for project")
		return p, err
	}

	if desc.Valid {
		p.Description = desc.String
	}

	sqlq = `
	SELECT remote.url, remote.ref
	FROM git_remotes AS remote
	INNER JOIN projects AS proj
	ON proj.id = remote.project_id
	WHERE proj.id = $1
	AND (
		proj.user_email = $2
		OR proj.group_name IN (
			SELECT mapping.group_name
			FROM user_groups AS mapping
			WHERE mapping.user_email = $3
		)
	);
	`

	rows, err := tx.Query(sqlq, id, user, user)
	if err != nil {
		logger.WithError(err).Debug("unable to query database")
		return types.Project{}, err
	}

	p.GitRemotes = []types.GitRemote{}
	for rows.Next() {
		logger.Debug("scanning row")

		var gr types.GitRemote
		err := rows.Scan(&gr.URL, &gr.Ref)
		if err != nil {
			logger.WithError(err).Debug("unable to get git remote from postgres")
			return p, err
		}

		p.GitRemotes = append(p.GitRemotes, gr)
	}
	if err := rows.Err(); err != nil {
		logger.WithError(err).Debug("got error scanning git remote rows")
	}

	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Debug("unable to commit transaction, rolling back")

		e := tx.Rollback()
		if e != nil {
			logger.WithError(err).Debug("couldn't roll back, aborting")
		}

		// The error from committing the transaction is probably more interesting
		// to the caller than the error from rolling back.
		return types.Project{}, err
	}

	return p, err
}

// DeleteProject deletes the project with the given id in the database.
func (pg *Postgres) DeleteProject(id string) error {
	// Leaving this unimplemented for now because it's not really useful.
	return errors.New("db method not implemented")
}

func (pg *Postgres) CreateGitRemote(user string, remote types.GitRemote) error {
	logger.Debug("creating git remote")

	tx, err := pg.db.Begin()
	if err != nil {
		logger.WithError(err).Debug("unable to create transaction")

		return err
	}

	// Since we're in a transaction anyways, it's safe to just query for authorization
	// separately from the actual database queries.
	sqlq := `
	SELECT COUNT(1)
	FROM projects AS proj
	WHERE proj.id = $1
	AND (
		proj.user_email = $2
		OR proj.group_name IN (
			SELECT mapping.group_name
			FROM user_groups AS mapping
			WHERE mapping.user_email = $3
		)
	);
	`

	logger.Debug("querying user authorization")

	var count int
	row := tx.QueryRow(sqlq, remote.ProjectID, user, user)
	err = row.Scan(&count)
	if err != nil {
		logger.WithError(err).Debug("unable to query project for authorization")

		errRollback := tx.Rollback()
		if errRollback != nil {
			logger.WithError(errRollback).Debug("unable to roll back either! (┛◉Д◉)┛彡┻━┻")
		}

		return err
	}

	if count == 0 {
		err := errors.New("user not authorized to create git remote on project")
		logger.WithError(err).Debug("unable to query project for authorization")

		errRollback := tx.Rollback()
		if errRollback != nil {
			logger.WithError(errRollback).Debug("unable to roll back either! (┛◉Д◉)┛彡┻━┻")
		}

		return err
	}

	// Past this point we know the passed in user ID is authorized to create
	// a git remote on the project so we don't need the auth checks in the
	// queries themselves.

	logger.Debug("querying for duplicate git remotes")

	sqlq = `
	SELECT COUNT(1)
	FROM git_remotes AS remote
	WHERE remote.url = $1 AND remote.ref = $2 AND remote.project_id = $3;
	`

	row = tx.QueryRow(sqlq, remote.URL, remote.Ref, remote.ProjectID)
	err = row.Scan(&count)
	if err != nil {
		logger.WithError(err).Debug("unable to query git remotes for duplicates")
	}

	if count > 0 {
		logger.WithError(types.ErrGitRemoteExists).Debug("found duplicate git remote")

		errRollback := tx.Rollback()
		if errRollback != nil {
			logger.WithError(errRollback).Debug("unable to roll back either! (┛◉Д◉)┛彡┻━┻")
		}

		return types.ErrGitRemoteExists
	}

	logger.Debug("inserting git remote into database")

	sqlq = `
	INSERT INTO git_remotes (url, ref, project_id)
	VALUES
		($1, $2, $3);
	`

	_, err = tx.Exec(sqlq, remote.URL, remote.Ref, remote.ProjectID)
	if err != nil {
		logger.WithError(err).Debug("unable to save git remote to database")

		errRollback := tx.Rollback()
		if errRollback != nil {
			logger.WithError(errRollback).Debug("unable to roll back either! (┛◉Д◉)┛彡┻━┻")
		}

		return err
	}

	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Debug("unable to commit transaction, rolling back")

		errRollback := tx.Rollback()
		if errRollback != nil {
			logger.WithError(errRollback).Debug("unable to roll back either! (┛◉Д◉)┛彡┻━┻")
		}
	}
	return err
}

// Authenticate checks to see if the user and pass are valid in the
// database. It returns an error if they aren't.
func (pg *Postgres) Authenticate(user, pass string) error {
	logger := logger.WithField("user", user)
	logger.Debug("authenticating user")

	sqlq := `
	SELECT password
	FROM users
	WHERE users.email = $1
	`

	cryptpass := []byte{}
	err := pg.db.QueryRow(sqlq, user).Scan(&cryptpass)
	if err != nil {
		logger.WithError(err).Debug("unable to query row")
		if err == sql.ErrNoRows {
			return errors.New("unable to authenticate user in database")
		}
	}

	err = bcrypt.CompareHashAndPassword(cryptpass, []byte(pass))
	if err != nil {
		logger.WithError(err).Debug("unable to authenticate")
		return errors.New("unable to authenticate user in database")
	}

	return nil
}
