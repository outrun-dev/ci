CREATE TABLE projects (
    id SERIAL NOT NULL UNIQUE,
    name varchar(255),
    description varchar(255),

    user_email varchar(255) NOT NULL,
    group_name varchar(255) NOT NULL,
    permissions smallint NOT NULL
);

CREATE TABLE git_remotes (
    url varchar(255) NOT NULL,
    ref varchar(255) NOT NULL,

    project_id INTEGER NOT NULL,

    FOREIGN KEY (project_id) REFERENCES projects(id),
    PRIMARY KEY(url, ref),
    UNIQUE(url, ref)
);

CREATE TABLE groups (
    name varchar(255) NOT NULL UNIQUE,

    PRIMARY KEY (name)
);

CREATE TABLE users (
    email varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL UNIQUE,
    name varchar(255) NOT NULL,

    PRIMARY KEY (email)
);

CREATE TABLE user_groups (
    user_email varchar(255) NOT NULL,
    group_name varchar(255) NOT NULL,

    FOREIGN KEY (group_name) REFERENCES groups(name),
    FOREIGN KEY (user_email) REFERENCES users(email),

    PRIMARY KEY (user_email, group_name)
);

-- The default group is what all users are created in originally
-- until they are added to groups, if any.
INSERT INTO groups (name)
VALUES
    ('default');

-- TODO: Use whatever signup method is implemented to create these on deploy.

INSERT INTO users (email, password, name)
VALUES
    (
        'outrun@local',
        -- "foobar"
        '$2a$10$4AAs.r.ouoeMB2r231VVTepsNMoqsQcLBxQJsZP1./EzA5hK1ufS2',
        'outrun'
    );

INSERT INTO user_groups (user_email, group_name)
VALUES
    ('outrun@local', 'default');
