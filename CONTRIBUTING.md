# Contributing Guide

## Development Environment

You need Docker, Minikube and Go installed.

## Building

To build the container, first build the binary:

```
# Disabling CGO is necessary to run correctly in Alpine.
CGO_ENABLED=0 go build -o build/ciapi ./cmd/ciapi/...
```

If building for minikube, set up the environment:

```
eval $(minikube docker-env)
```

Then, build the container image:

```
docker build -t outrun-ci/ciapi .
```

## Working Locally

Local work depends on Minikube.

To begin, spin up a local Kubernetes cluster:

```
minikube start
```

In another terminal window, mount the local filesystem to the VM. This
will be used to bootstrap the PostgreSQL container.

```
minikube mount $(pwd):/mnt/repo
```

Set the `kubectl` configuration to use the right namespace:

```
kubectl config set-context $(kubectl config current-context) --namespace=outrun
```

Some default RBAC configuration is necessary to be able to run everything:

```
kubectl apply -f manifests/rbac.yaml
```

### Spinning Up the Database

Volumes for PostgreSQL need to be created and bound beforehand for the
container to bootstrap correctly.

Create the volumes:

```
kubectl apply -f manifests/pq/volumes.yaml
```

**NOTE:** Wait for the persistent volume claim(s) to be bound before
moving on to the next step. If not, PostgreSQL will fail to bootstrap
using the bootstrap script in the repository.

Finally, spin up PostgreSQL:

```
kubectl apply -f manifests/pq/db.yaml
```

If the bootstrap worked correctly, you should see something like this
in the PostgreSQL container's logs:

```
/usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/bootstrap.sql
CREATE TABLE
CREATE TABLE
CREATE TABLE
CREATE TABLE
INSERT 0 1
INSERT 0 1
INSERT 0 1
```

### Spinning Up `ciapi`

Apply the manifest for your local environment:

```
kubectl apply -f manifests/local.yaml
```

You can get the service URL from Minikube using its `services` command:

```
# Shows you all services exposed and their external URLs if any
minikube service list
```

The flow for making changes ends up being:

```
# This is output from `history`
2081  CGO_ENABLED=0 go build -o build/ciapi 
2082  docker build -t outrun-ci/ciapi .
2083  kubectl delete pods/ciapi-srv
2084  kubectl apply -f manifests/local.yaml 
```
