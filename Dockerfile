FROM alpine:3.9

ADD build/ciapi /bin/ciapi

ENTRYPOINT [ "/bin/ciapi" ]